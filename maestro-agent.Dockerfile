############################
# Maestro Agent Docker Image
############################

FROM zerossl/client:latest

USER root

RUN apk add --no-cache python3 py3-requests

COPY code/maestro /usr/lib/python3.5/maestro

COPY maestro-agent/docker-cmd.sh /docker-cmd.sh
RUN chmod 744 /docker-cmd.sh

ENTRYPOINT ["sh", "/docker-cmd.sh"]
