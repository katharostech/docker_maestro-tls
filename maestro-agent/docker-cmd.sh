# Set default values for environment variables
interval=${interval-24}
log_level=${log_level-INFO}

# Use `eval` to expand variables and run the maestro agent
eval "exec python3 -m maestro --log-level $log_level agent --apply-at-interval $interval $rancher_server $load_balancer_id $target_service_id $port_rules"
