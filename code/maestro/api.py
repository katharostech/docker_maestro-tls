import copy
import logging
import json
import string
from urllib.parse import urlparse, urljoin

from enum import Enum, unique

import requests


logger = logging.getLogger(__name__)


@unique
class Protocol(Enum):
    """A network protocol"""
    HTTP = 1
    TCP = 2
    HTTPS = 3
    TLS = 4
    SNI = 5
    UDP = 6


class RancherAPI:
    """Allows access to a Rancher server's rest API"""

    def __init__(
        self, host, access_key, secret_key, port=None, protocol=Protocol.HTTP,
        api_version='v1'):
        """Initialize a `RancherAPI`
        
        :arg host: The hostname of the Rancher server to connect to.
        :arg type: string
        :arg access_key: The access key to connect to the Rancher server with.
        :arg type: string
        :arg secret_key: The secret key to connect to the Rancher server with.
        :arg port: The port to connec to the Rancher server on.
        :arg type: int
        :arg protocol: The protocol with which to connect to the Rancher server
        :arg type: `Protocol`. Must be either `Protocol.HTTP` or
                   `Protocol.HTTPS`
        :arg api_version: The version of the Rancher API to use.
        :arg type: string
        """
        self.host = host
        self.access_key = access_key
        self.secret_key = secret_key
        if port is None and protocol == Protocol.HTTP:
            self.port = 80
        elif port is None and protocol == Protocol.HTTPS:
            self.port = 443
        else:
            self.port = port
        self.protocol = protocol
        self.api_version = api_version

    @property
    def host(self):
        return self._host

    @host.setter
    def host(self, value):
        self._host = value

    @property
    def access_key(self):
        return self._access_key

    @access_key.setter
    def access_key(self, value):
        self._access_key = value

    @property
    def secret_key(self):
        return self._secret_key

    @secret_key.setter
    def secret_key(self, value):
        self._secret_key = value

    @property
    def port(self):
        return self._port

    @port.setter
    def port(self, value):
        if value is not None:
            self._port = int(value)
        else:
            self._port = None

    @property
    def protocol(self):
        return self._protocol

    @protocol.setter
    def protocol(self, value):
        if value in (Protocol.HTTP, Protocol.HTTPS):
            self._protocol = value
        else:
            raise ValueError("protocol must be either Protocol.HTTP or "
                             "Protocol.HTTPS")

    @property
    def api_version(self):
        return self._api_version

    @api_version.setter
    def api_version(self, value):
        self._api_version = value

    def get_resource_endpoint(self, resource_type=None, resource_id=None,
        endpoint=None):
        """Get the API endpoint URL of a Rancher resource

        :arg resource_type: The type of Rancher resource to get
        :arg type: string
        :arg resource_id: The id of the resource to get
        :arg type: string
        :arg endpoint: A Rancher API endpoint as found in the the API 'links'
                       This will be ignored if resource_type and resource_id
                       are set.
        :arg type: A Rancher API link string

        Usage::
            
            >>> rancher_api = RancherAPI(
            ...     'host.com', 'accesskey', 'secretkey',
            ...     port=8080, protocol=Protocol.HTTPS, api_version='v2-beta')
            >>> rancher_api.get_resource_endpoint(
            ...     'service', '1s217')
            'https://accesskey:secretkey@host.com:8080/v2-beta/service/1s217'
            >>> rancher_api.get_resource_endpoint(
            ...     endpoint='http://host.com:9090/v1/schemas/account')
            'http://accesskey:secretkey@host.com:9090/v1/schemas/account'
        """
        netloc_template = string.Template(
            "${protocol}://${access_key}:${secret_key}@${host}:${port}")
        relative_path = ""

        if resource_type is not None and resource_id is not None:
            relative_path = '/'.join(
                [self.api_version, resource_type, resource_id])
            netloc = netloc_template.substitute(
                protocol=self.protocol.name.lower(),
                access_key=self.access_key,
                secret_key=self.secret_key,
                host=self.host,
                port=self.port)

        elif endpoint is not None:
            url = urlparse(endpoint)

            netloc = netloc_template.substitute(
                protocol=url.scheme,
                access_key=self.access_key,
                secret_key=self.secret_key,
                host=url.hostname,
                port=self.port)
            relative_path = url.path

        else:
            raise ValueError(
                "Method requires both resource_type and resource_id or "
                "else endpoint needs to be not None")

        return urljoin(netloc, relative_path)


    def get_resource(self, resource_type=None, resource_id=None,
        endpoint=None):
        """Get a dictionary representing a resource in the v2-beta Rancher API.

        :arg rancher_server: The rancher server in the format:
                             http[s]://accesskey:secretkey@domain.com
        :arg type: string
        :arg resource_type: The type of resource to get corresponding to the
                            API endpoint in version 2 the the Rancher API.
        :arg type: string
        :arg resource_id: The id of the resource to get
        :arg type: string
        :arg endpoint: The full API endpoint URL to update. If specified,
                       rancher_server, resource_type, and resource_id will be
                       ignored.
        :return: The Rancher API resource
        :rtype: A dictionary representation of the resource's json as returned
                from `json.loads`
        """
        if resource_type is not None and resource_id is not None:
            url = self.get_resource_endpoint(resource_type, resource_id)
        elif endpoint is not None:
            url = self.get_resource_endpoint(endpoint=endpoint)
        else:
            raise ValueError(
                "Method requires both resource_type and resource_id or "
                "else endpoint needs to be not None")

        return json.loads(requests.get(url).text)


    def update_resource(
        self, data, resource_type=None, resource_id=None, endpoint=None):
        """Update a v2-beta Rancher API resource with new data
     
        :arg data: The data to update on the resource
        :arg type: A `dict` where the keys are the fields you wish to update on
                   the resource. Values may be `dict`s as well if fields are 
                   nested.
        :arg type: string
        :arg resource_type: The type of resource to get corresponding to the
                            API endpoint in version 2 the the Rancher API.
        :arg type: string
        :arg resource_id: The id of the resource to get
        :arg type: string
        :arg endpoint: The full API endpoint URL to update. If specified,
                       rancher_server, resource_type, and resource_id will be
                       ignored.
        :arg type: string
        :arg returns: The updated Rancher resource
        :arg type: dict
        """
        if resource_type is not None and resource_id is not None:
            url = self.get_resource_endpoint(resource_type, resource_id)
        elif endpoint is not None:
            url = self.get_resource_endpoint(endpoint=endpoint)
        else:
            raise ValueError(
                "Method requires both resource_type and resource_id or "
                "else endpoint needs to be not None")

        return json.loads(requests.put(url, data=json.dumps(data)).text)


class PortRule:
    """A Rancher PortRule used for load balancer config
    
    Properties mirror those in the Rancher api v2 with underscores between
    words instead of capitals.  """ 
    def __init__(self, backend_name=None, hostname=None, path=None,
                 priority=None, protocol=Protocol.HTTP, selector=None,
                 service_id=None, source_port=None, target_port=None):
        """Initialize a Port Rule

        Parameter meanings are the same as the corresponding properties in the
        Rancher API v2.
        """
        self.backend_name = backend_name
        self.hostname = hostname
        self.path = path
        self.priority = priority
        self.protocol = protocol
        self.selector = selector
        self.service_id = service_id
        self.source_port = source_port
        self.target_port = target_port

    @property
    def backend_name(self):
        return self._backend_name

    @backend_name.setter
    def backend_name(self, value):
        self._backend_name = value

    @property
    def hostname(self):
        return self._hostname

    @hostname.setter
    def hostname(self, value):
        self._hostname = value

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, value):
        self._path = value

    @property
    def priority(self):
        return self._priority

    @priority.setter
    def priority(self, value):
        if value is not None:
            self._priority = int(value)
        else:
            self._priority = None

    @property
    def protocol(self):
        return self._protocol

    @protocol.setter
    def protocol(self, value):
        if value in Protocol:
            self._protocol = value
        else:
            raise TypeError("Cannot assign value: "
                            "not a maestroagent.rancherutils.Protocol object")

    @property
    def selector(self):
        return self._selector
    
    @selector.setter
    def selector(self, value):
        self._selector = value
    
    @property
    def service_id(self):
        return self._service_id

    @service_id.setter
    def service_id(self, value):
        self._service_id = value

    @property
    def source_port(self):
        return self._source_port

    @source_port.setter
    def source_port(self, value):
        if value is not None:
            self._source_port = int(value)
        else:
            self._source_port = None

    @property
    def target_port(self):
        return self._target_port

    @target_port.setter
    def target_port(self, value):
        if value is not None:
            self._target_port = int(value)
        else:
            self._target_port = None
    
    def __iter__(self):
        """Iterator for `dict` conversion
        
        Using `dict()` on a `PortRule` will return a dictionary in the format
        expected by a Rancher "portRule" resource.
        """
        yield ('type', 'portRule')
        if self.backend_name is not None: yield (
            'backendName', self.backend_name)
        if self.hostname is not None: yield ('hostname', self.hostname)
        if self.path is not None: yield ('path', self.path)
        if self.priority is not None: yield ('priority', self.priority)
        yield ('protocol', self.protocol.name.lower())
        if self.selector is not None: yield ('selector', self.selector)
        if self.service_id is not None: yield ('serviceId', self.service_id)
        if self.source_port is not None: yield ('sourcePort', self.source_port)
        if self.target_port is not None: yield ('targetPort', self.target_port)


class PortRuleManager:
    """Used to apply given portRules to a Rancher load balancer

    Checks a Rancher load balancer and ensures that the given port rules exist
    and have not been modified. Traffic is directed to a single service in the
    Rancher environment. If traffic is to be directed to multiple services,
    multiple PortRuleManager objects must be created.

    If a difference in the passed-in configuration is detected, or if the port
    rules do not exist, they will be updated or created to match the passed-in
    configuration.

    In addition, the PortRuleManager will, when necessary, automatically
    generate valid HTTPS certificats using the zeroSSL commandline utility.
    This specific functionality requires having the zeroSSL comandline utility,
    and an nginx web-server, that this script has access to, installed on the
    host. If the required programs are not detected, no certs will be
    generated; the default cert for the load balancer will be used if it
    exists. If there is no default cert, HTTPS/TLS port rules cannot be
    created.

    .. Note::
        Although you may still create TLS port rules, the `PortRuleManager`
        does not currently support generating TLS certificates.

        TLS certificates can still be created manually and added to the load
        balancer through the UI if desired; otherwise, the default cert will be
        used.
    """

    def __init__(self, rancher_api, load_balancer_id, target_service_id,
                 port_rules):
        """Initialize PortRuleManager with desired configuration.
        
        :arg rancher_api: The `rancherAPI` that will be used to communicate to
                          the Rancher server.
        :arg type: `rancherAPI` with a version 2 api, i.e. 'v2' or 'v2-beta'.
        :load_balancer_id: The resource id of the load balancer service to
                           apply configuration to.
        :arg type: string
        :target_service_id: The service id to direct network traffic to.
        :arg type: string
        :port_rules: An iterable of `PortRule`s to apply to the load balancer.
                     The `PortRule.service_id` may be left out as the
                     `PortRuleManager.target_service_id` attribute will be used
                     instead. `PortRule.backend_name` will also be generated by
                     the `PortRuleManager` automatically.
        :arg type: Iterable of `PortRule`s
        """
        self.rancher_api = rancher_api
        self.load_balancer_id = load_balancer_id
        self.target_service_id = target_service_id
        self.port_rules = port_rules

        # Generate `backend_name`s and `service_id`s for each `PortRule`.
        for port_rule in self.port_rules:
            port_rule.service_id = self.target_service_id
            port_rule.backend_name = (
                "{port_rule.hostname}:{port_rule.source_port}"
                "->{port_rule.service_id}:{port_rule.target_port}"
                ).format(**vars())

    @property
    def rancher_api(self):
        return self._rancher_api

    @rancher_api.setter
    def rancher_api(self, value):
        if 'v2' in value.api_version:
            self._rancher_api = value
        else:
            raise ValueError(
                "rancher_api must be a version 2 api, i.e. 'v2' or 'v2-beta', "
                "not {!r}.".format(value.api_version))

    @property
    def load_balancer_id(self):
        return self._load_balancer

    @load_balancer_id.setter
    def load_balancer_id(self, value):
        self._load_balancer = value

    @property
    def target_service_id(self):
        return self._service

    @target_service_id.setter
    def target_service_id(self, value):
        self._service = value

    @property
    def port_rules(self):
        return self._port_rules

    @port_rules.setter
    def port_rules(self, value):
        self._port_rules = value

    def apply_port_rules(self):
        """Create/Update port rules on the load balancer."""
        logger.info("Applying port rules to load balancer")

        logger.debug("Getting load balancer from Rancher server...")
        # The load balancer on the Rancher server as a dictionary
        load_balancer = self.rancher_api.get_resource(
            "loadbalancerservice",
            self.load_balancer_id)
        logger.debug("Done getting load balancer.")

        # The update to put to the load balancer if necessary.
        update = {}
        update['lbConfig'] = load_balancer['lbConfig']
        update_port_rules = update['lbConfig']['portRules']
        update['launchConfig'] = load_balancer['launchConfig']
        # The set of ports that will be exposed on the host
        exposed_ports = set(update['launchConfig']['ports'])

        # Dictionary of the dict representations of our `PortRules` indexed by
        # backend_name. We will remove these from the dict as we apply
        # the updates.
        backends = {}

        # Index our port rules by backend name and store in `backends`
        for port_rule in self.port_rules:
            backends[port_rule.backend_name] = dict(port_rule)

        # See if our port rules are already on the server and update them if
        # necessary.
        logger.debug("Checking current port rules")
        for port_rule in update_port_rules.copy():
            try:
                backend_name = port_rule['backendName']
            except KeyError:
                continue
            # If the backend is one of ours...
            if backend_name in backends:
                # Add backend to `update` if it does not match version
                # currently on server.
                if not port_rule == backends[backend_name]:
                    logger.info(
                        "Backend, %r, EXISTS and is NOT EQUAL to "
                        "defined rule. Adding to update.", backend_name)
                    # Remove old port rule.
                    update_port_rules.pop(update_port_rules.index(port_rule))
                    # Add new port rule.
                    update_port_rules.append(backends.pop(backend_name))
                    exposed_ports.add("{0}:{0}/tcp".format(
                        port_rule["sourcePort"]))
                else:
                    logger.info(
                        "Backend, %r, EXISTS and IS EQUAL. Does not need "
                        "update.", backend_name)
                    backends.pop(backend_name)
        
        # Add every backend not already present to the update
        for backend in backends.copy():
            port_rule = backends.pop(backend)
            update_port_rules.append(port_rule)
            exposed_ports.add("{0}:{0}/tcp".format(port_rule['sourcePort']))
            logger.info(
                "Backend, %r, DOES NOT EXIST. Adding "
                "to update.", backend)

        # Add exposed port updates if necessary
        if list(exposed_ports) != load_balancer['launchConfig']['ports']:
            update['launchConfig']['ports'] = list(exposed_ports)

        # Apply the update
        result = self.rancher_api.update_resource(
            update, endpoint=load_balancer['links']['self'])
        if result['type'] == 'error':
            raise Exception(result['message'])
        logger.info("All backends up to date.")

    def remove_port_rules(self):
        """Remove previously applied port rules from the load balancer"""
        logger.info("Removing port rules from load balancer")

        logger.debug("Getting load balancer from Rancher server...")
        # The load balancer on the Rancher server as a dictionary
        load_balancer = self.rancher_api.get_resource(
            "loadbalancerservice",
            self.load_balancer_id)
        logger.debug("Done getting load balancer.")

        # Index our port rules by backend name and store in `backends`
        backends = {}
        for port_rule in self.port_rules:
            backends[port_rule.backend_name] = dict(port_rule)

        # The update to apply to the load balancer
        update = {}
        update['lbConfig'] = load_balancer['lbConfig']
        update_port_rules = update['lbConfig']['portRules']
        update['launchConfig'] = load_balancer['launchConfig']
        # The set of ports that will be exposed on the host
        exposed_ports = set({})

        for port_rule in update_port_rules.copy():
            try:
                backend_name = port_rule['backendName']
            except KeyError:
                backend_name = ""
            if backend_name in backends:
                # Remove the port rule
                update_port_rules.pop(update_port_rules.index(port_rule))
                logger.info("Removed port rule: %s", backend_name)
            else:
                exposed_ports.add("{0}:{0}/tcp".format(
                    port_rule['sourcePort']))

        update['launchConfig']['ports'] = list(exposed_ports)

        # Apply the update
        result = self.rancher_api.update_resource(
            update, endpoint=load_balancer['links']['self'])
        if result['type'] == 'error':
            raise Exception(result['message'])
        logger.info("Port rules removed from load balancer.")

