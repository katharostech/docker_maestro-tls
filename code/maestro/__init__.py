#!/usr/bin/python3
import argparse
import logging
import sys
import signal
import textwrap
from time import sleep
from urllib.parse import urlparse 
from maestro.api import PortRule, PortRuleManager, Protocol, RancherAPI


logger = logging.getLogger(__name__)


def parse_arguments(raw_args):
    """Define commandline arguments and parse input."""
    # Top level parser
    parser = argparse.ArgumentParser(
        prog="maestro",
        description="Run an instance of a Maestro TLS agent/conductor.") 

    # Top level optional arguments
    parser.add_argument(
        "-q", "--quiet",
        help="Don't output logs to stderr",
        action="store_true")
    parser.add_argument(
        "-l", "--log-level",
        default="WARNING",
        choices=["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"])
    parser.add_argument("-f", "--log-file")

    # Add the subparsers
    subparsers = parser.add_subparsers(
        title="Subcommands",
        description="You can run either a conductor or an agent.\n",
        dest="subparser",
        help="You can only have one conductor and there must be one agent per "
             "backend")

    # The conductor sub-command
    agent_parser = subparsers.add_parser(
        'conductor',
        help="Run a Maestro TLS conductor instance")

    # TODO: add the conductor implementation

    # The agent sub-command
    agent_parser = subparsers.add_parser(
        'agent',
        help="Run a Maestro TLS agent instance. By default the agent will "
             "make sure the port rules are in place on the load balancer at "
             "an interval and will remove the port rules when it exits.")
    
    # Positional arguments
    agent_parser.add_argument(
        "rancher_server",
        help="The Rancher server to connect to in the format:\n" +
             "http[s]://accesskey:secretkey@rancherserver")
    agent_parser.add_argument(
        "load_balancer_id",
        help="The ID of the Rancher load ballancer service to add port " +
             "rules to. Example: 1s271")
    agent_parser.add_argument(
        "target_service_id",
        help="The ID of the Rancher service to direct traffic to. " +
             "Example: 1s172")
    agent_parser.add_argument(
        "port_rules",
        help="The port rules to create on the load balancer in the format:\n" +
             "'protocol://domain.com:source_port/path->target_port'\n" +
             "Multiple port rules should be comma-sepparated.",
        nargs='+',
        metavar="port_rule")

    # Optional Arguments
    agent_parser.add_argument(
        "--redirect-http",
        "-r",
        help="Redirect http traffic to https trafic on the same host. You "
             "must supply the id of the 'http-redirect' service of the "
             "maestro-tls Rancher stack.",
        dest="http_redirect_service_id")

    mode_group = agent_parser.add_mutually_exclusive_group()
    mode_group.add_argument(
        "--apply-once",
        help="Apply the port rules to the load balancer and exit.",
        action="store_true")
    mode_group.add_argument(
        "--remove-once",
        help="Remove the port rules from the load balancer and exit.",
        action="store_true") 
    mode_group.add_argument(
        "--apply-at-interval",
        "-i",
        help="Apply the port rules to the load balancer on an interval "
             "measured in hours.",
        dest="interval",
        type=float,
        default=12,
        metavar="INTERVAL")

    # Parse arguments
    args = parser.parse_args(raw_args)

    # Print usage if the user has not entered either sub-command
    if args.subparser is None:
        parser.print_usage()
        parser.exit(status=1)

    return args


def parse_port_rules(str_port_rules):
    """Parses the port rules as expected on the commandline.

    Parses a list of port rules in the format::

        "protocol://domainname.com:source_port/path->target_port"
    
    Multiple port rules may be comma-separated.

    :Returns: A list of `PortRule`s
    """ 
    # Remove spaces
    str_port_rules = str_port_rules.replace(" ", "")

    port_rules = []
    
    for str_port_rule in str_port_rules.split(','):
        str_url, str_target_port = str_port_rule.split('->')

        url = urlparse(str_url)
        target_port = int(str_target_port)

        new_rule = PortRule()
        new_rule.hostname = url.hostname
        new_rule.protocol = getattr(Protocol, url.scheme.upper())
        new_rule.path = url.path.strip("/")
        new_rule.source_port = url.port
        new_rule.target_port = target_port

        port_rules.append(new_rule)

    return port_rules


def configure_logging(log_level=logging.WARNING, log_file=None, quiet=None):
    """Configure log level and add log handlers."""

    # Set the level based on the commandline argument
    logger.setLevel(log_level)

    # Set the default formatter
    log_formatter = logging.Formatter(
        fmt="%(asctime)s: %(name)s - %(levelname)s - %(message)s")

    # Add a NullHander to prevent a message being displayed if the command is
    # run with --quiet
    logger.addHandler(logging.NullHandler())

    # Add the stderr log handler
    if quiet is not True:
        stderr_handler = logging.StreamHandler()
        stderr_handler.setFormatter(log_formatter)
        logger.addHandler(stderr_handler)

    # Add the log file handler
    if log_file is not None:
        logfile_handler = logging.FileHandler(log_file)
        logfile_handler.setFormatter(log_formatter)
        logger.addHandler(logfile_handler)


def get_rancher_api(connection_url, api_version='v1'):
    url = urlparse(connection_url)

    return RancherAPI(
        url.hostname,
        url.username,
        url.password,
        url.port,
        getattr(Protocol, url.scheme.upper()),
        api_version)


def run_maestro_agent(args):
    """Run the maestro agent

    :arg args: the commandline arguments parsed by argparse
    :arg type: an `argparse.Namespace`
    """
    logger.debug("Starting agent sub-command...")
    logger.debug("Rancher Server: %r", args.rancher_server)

    # Obtain a RancherAPI
    logger.debug("Obtaining RancherAPI...")
    rancher_api = get_rancher_api(
        args.rancher_server,
        api_version='v2-beta')
    logger.debug("Obtained RancherAPI.")

    # Parse Port Rules
    port_rules = parse_port_rules(''.join(args.port_rules))

    # Initialize a PortRuleManager
    logger.debug("Initializing PortRuleManager.")
    logger.debug("Load Balancer: %r", args.load_balancer_id)
    logger.debug("Target Service: %r", args.target_service_id)

    # Log PortRules
    for i in range(len(port_rules)):
        logger.debug("Port Rule %i:", i+1)
        logger.debug(
            textwrap.indent("Hostname: %r", "    "),
            port_rules[i].hostname)
        logger.debug(
            textwrap.indent("Path: %r", "    "),
            port_rules[i].path)
        logger.debug(
            textwrap.indent("Protocol: %r", "    "),
            port_rules[i].protocol.name)
        logger.debug(
            textwrap.indent("Source Port: %r", "    "),
            port_rules[i].source_port)
        logger.debug(
            textwrap.indent("Target Port: %r", "    "),
            port_rules[i].target_port)

    port_rule_manager = PortRuleManager(
        rancher_api,
        args.load_balancer_id,
        args.target_service_id,
        port_rules)
    logger.debug("PortRuleManager initialized.")

    # Add the signal handler
    def signal_handler(signum, frame):
        logger.info("---- SIGTERM caught ----")
        port_rule_manager.remove_port_rules()
        sys.exit(0)
    signal.signal(signal.SIGTERM, signal_handler)

    # Apply the port rules to the load balancer
    if args.apply_once is True:
        port_rule_manager.apply_port_rules()
    elif args.remove_once is True:
        port_rule_manager.remove_port_rules()
    else:
        logger.info("Applying port rules on %s hour interval.", args.interval)
        try:
            while(True):
                port_rule_manager.apply_port_rules()
                sleep(args.interval * 60 * 60)
        except KeyboardInterrupt:
            logger.info("---- Keyboard interrupt detected ----")
            port_rule_manager.remove_port_rules()


def main(main_args=None):
    """Execute maestro as it is run on the commandline."""
    if main_args is None:
        main_args = sys.argv[1:]
    
    args = parse_arguments(main_args)

    configure_logging(
        log_level=getattr(logging, args.log_level),
        log_file=args.log_file,
        quiet=args.quiet)

    logger.debug("Logging Configured")

    if args.subparser == 'agent':
        run_maestro_agent(args)


if __name__ == '__main__':
    sys.exit(main())

